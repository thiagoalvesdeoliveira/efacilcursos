# Efacil Cursos

> Um sistema de INSTRUTOR ESCOLAR, onde esse sistemas tem como fundamento facilitar o acesso a pessoas que não possuem conhecimento sobre os sistemas e softwares mais utilizados nos trabalhos básico de uma empresa no seu dia a dia, esses programas seriam tais como Excel, Power Point, Word, Auto Cad, Revit, Coredraw entre outros, ou Matemática Fisica, Quimica.


# Executar a Aplicação

Pré Requisitos:

- [x] VS Studio 2019
- [x] MSSQL
- [x] .NET CORE 2.1

# Passo a Passo

Para conseguir executar e testar a plataforma, é antes necessário criar um DataBase com o nome EfacilCursos, e executar os scripts presentes na pasta "SCRIPTS_BANCO" seguindo ordem descrita nos nomes dos arquivos.

Após a execusão deve-se alterar a string de conxão de acordo com o seu PC-Name, no arquivo `startup.cs`

### Tecnologias utilizadas
- [x] Asp.net Core
- [x] C#
- [x] SQL Server
