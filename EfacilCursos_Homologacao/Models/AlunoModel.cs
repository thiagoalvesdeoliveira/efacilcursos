﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EfacilCursos_Homologacao.Models
{
    public class AlunoModel
    {
        [Key]
        public Guid Id_Aluno { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tamanho máximo de Nome é 100 caracteres")]
        public string Nome { get; set; }

        [Required]
        [StringLength(320, ErrorMessage = "Tamanho máximo de Email é 320 caracteres")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tamanho máximo de Senha é 100 caracteres")]
        public string Senha { get; set; }

        [Required]
        [StringLength(11, ErrorMessage = "Tamanho máximo de CPF é 11 caracteres")]
        public string CPF { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tamanho máximo de Estado é 100 caracteres")]
        public string Estado { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tamanho máximo de Telefone é 100 caracteres")]
        public string Telefone { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tamanho máximo de Endereço é 100 caracteres")]
        public string Endereco { get; set; }

        [Required]
        [StringLength(18, ErrorMessage = "Tamanho máximo de CEP é 18 caracteres")]
        public string Cep { get; set; }

        [Required]
        [StringLength(18, ErrorMessage = "Tamanho máximo de Sexo é 18 caracteres")]
        public string Sexo { get; set; }

        [Required]
        [StringLength(600, ErrorMessage = "Tamanho máximo de Url é 600 caracteres")]
       
        public DateTime DataCadastro { get; set; }
        public DateTime DataNascimento { get; set; }
        public bool Ativo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Enumerable.Empty<ValidationResult>();
        }
    }
}
