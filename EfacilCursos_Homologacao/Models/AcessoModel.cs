﻿using System.ComponentModel.DataAnnotations;

namespace EfacilCursos_Homologacao.Models
{
    public class AcessoModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Senha { get; set; }
    }
}
