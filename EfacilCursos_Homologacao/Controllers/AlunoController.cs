﻿using EfacilCursos_Homologacao.Data;
using EfacilCursos_Homologacao.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace EfacilCursos_Homologacao.Controllers
{
    public class AlunoController : Controller
    {
        IHostingEnvironment _appEnvironment;

        private AppDbContext _Context;

        public AlunoController(AppDbContext context, IHostingEnvironment env)
        {
            _Context = context;
            _appEnvironment = env;
        }       

        public ActionResult Usuario()
        {
            return View();
        }

        public ActionResult AlunoAdd(int? id)
        {
            return View(new AlunoModel());
        }

        
        public IActionResult UsuarioGrid(string search)
        {
            return PartialView(string.IsNullOrWhiteSpace(search) ? _Context.Usuarios.AsQueryable() : _Context.Usuarios.Where(c =>
                c.Nome.Contains(search) ||
                c.Email.Contains(search)));
        }
               
        public ActionResult UsuarioEdit(int? id)
        {
            if (id > 0)
            {
                return View(_Context.Usuarios.Find(id));
            }
            else
            {
                return View(new AlunoModel());
            }
        }
               
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsuarioEdit(AlunoModel Aluno)
        {
            try
            {               
                var validacao = _Context.Usuarios.FirstOrDefault(c => c.Email == Aluno.Email);

                if (validacao != null)
                {
                    ViewBag.Messagen = "Não foi possivel cadastrar Email, já é cadastrado na nossa base";
                    return RedirectToAction("HomeCursos");
                }
                else
                {
                    Aluno.Ativo = true;
                    Aluno.DataCadastro = DateTime.Now;                  

                    _Context.Usuarios.Add(Aluno);
                    _Context.SaveChanges();
                }
                return RedirectToAction("HomeCursos");
               
            }
            catch (Exception)
            {
                return View(Aluno);
            }
        }
    }
}
