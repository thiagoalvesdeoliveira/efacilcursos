﻿using EfacilCursos_Homologacao.Data;
using EfacilCursos_Homologacao.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace EfacilCursos_Homologacao.Controllers
{
    public class AcessoController : Controller
    {
        IHostingEnvironment _appEnvironment;

        private AppDbContext _Context;

        public AcessoController(AppDbContext context, IHostingEnvironment env)
        {
            _Context = context;
            _appEnvironment = env;
        }

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Entrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(AcessoModel model)
        {

            var usuario = _Context.Usuarios.FirstOrDefault(c => c.Email == model.Email && c.Senha == model.Senha);

            if (usuario == null)
            {
                ViewBag.Messagen = "Usúario ou senha invalido.";
                return View(model);
            }

            return RedirectToAction("Index");

        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Entrar");
        }
    }
}
