﻿using EfacilCursos_Homologacao.Models;
using Microsoft.EntityFrameworkCore;

namespace EfacilCursos_Homologacao.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<AlunoModel> Usuarios { get; set; }

        //public DbSet<Cursos> Cursos { get; set; }

        //public DbSet<Professor> Professores { get; set; }

        //public DbSet<Aulas> Aulas { get; set; }

        //public DbSet<AlunoAddCurso> AlunoAddCursos { get; set; }
    }
}
