﻿const deleteCourseBtns = document.querySelectorAll('#delete-course');
const disableCourseBtns = document.querySelectorAll('#disable-course');
const activateCourseBtns = document.querySelectorAll('#activate-course');
const deleteModalContainer = document.querySelector('.delete-modal-container');
const disableModalContainer = document.querySelector('.disable-modal-container');
const activateModalContainer = document.querySelector('.activate-modal-container');
const textArea = document.querySelector('.reason-container > textarea');
const confirmDelete = document.getElementById('confirm-delete-modal-btn');
const noDelete = document.getElementById('no-delete-modal-btn');
const confirmDisable = document.getElementById('confirm-disable-modal-btn');
const noDisable = document.getElementById('no-disable-modal-btn');
const confirmActivate = document.getElementById('confirm-activate-modal-btn');
const noActivate = document.getElementById('no-activate-modal-btn');
const noConfirmBtns = [noDelete, noDisable, noActivate];
const disableCourseBtnModel = document.createElement('button');
const activateCourseBtnModel = document.createElement('button');
const radios = document.querySelectorAll('input[name=time]');
disableCourseBtnModel.setAttribute('type', 'button');
disableCourseBtnModel.setAttribute('id', 'disable-course');
disableCourseBtnModel.innerHTML = '<i class="far fa-eye-slash"></i> Desabilitar';
activateCourseBtnModel.setAttribute('type', 'button');
activateCourseBtnModel.setAttribute('id', 'activate-course');
activateCourseBtnModel.innerHTML = '<i class="far fa-eye"></i> Ativar';

disableCourseBtnModel.addEventListener('click', () => {
	changeToDisabled(disableCourseBtnModel);

	disableModalContainer.style.display = 'flex';
	disableModalContainer.style.alignItems = 'center';
	disableModalContainer.style.justifyContent = 'center';
});

activateCourseBtnModel.addEventListener('click', () => {
	changeToActive(activateCourseBtnModel);

	activateModalContainer.style.display = 'flex';
	activateModalContainer.style.alignItems = 'center';
	activateModalContainer.style.justifyContent = 'center';
});

const changeToActive = (btn) => {
	confirmActivate.addEventListener('click', () => {
		btn.parentNode.insertBefore(disableCourseBtnModel, btn.nextSibling);
		btn.parentNode.removeChild(btn);
		activateModalContainer.style.display = 'none';
	});
};

const changeToDisabled = (btn) => {
	confirmDisable.addEventListener('click', () => {
		btn.parentNode.insertBefore(activateCourseBtnModel, btn.nextSibling);
		btn.parentNode.removeChild(btn);
		disableModalContainer.style.display = 'none';
	});
};

// Botões para exclusão do curso
deleteCourseBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		deleteModalContainer.style.display = 'flex';
		deleteModalContainer.style.alignItems = 'center';
		deleteModalContainer.style.justifyContent = 'center';
	});
});

// Botões para desativação do curso
disableCourseBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		changeToDisabled(btn);

		disableModalContainer.style.display = 'flex';
		disableModalContainer.style.alignItems = 'center';
		disableModalContainer.style.justifyContent = 'center';
	});
});

// Botões para ativar do curso
activateCourseBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		changeToActive(btn);

		activateModalContainer.style.display = 'flex';
		activateModalContainer.style.alignItems = 'center';
		activateModalContainer.style.justifyContent = 'center';
	});
});

// Botões que não confirmam a ação
noConfirmBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		const modalContainer = btn.parentElement.parentElement.parentElement;

		modalContainer.style.display = 'none';
		textArea.value = '';
		confirmDelete.setAttribute('disabled', true);
	});
});

// Confirmar desativação
radios.forEach((radio) => {
	radio.addEventListener('change', () => {
		confirmDisable.removeAttribute('disabled');
	});
});

/* Contagem de caracteres e verificação do valor do campo motivo */
textArea.addEventListener('input', (e) => {
	const counter = textArea.nextElementSibling.querySelector('span');
	const valueLength = e.target.value.length;

	counter.innerText = valueLength;
});

textArea.addEventListener('change', (e) => {
	if (e.target.value.trim().length > 0) {
		confirmDelete.removeAttribute('disabled');
	} else {
		confirmDelete.setAttribute('disabled', true);
	}
});
