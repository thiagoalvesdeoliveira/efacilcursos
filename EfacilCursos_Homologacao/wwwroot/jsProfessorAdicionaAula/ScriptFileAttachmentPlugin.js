﻿$(document).ready(function () {

	$(document).on('change', '.up', function () {
		var id = $(this).attr('id');
		var profilePicValue = $(this).val();
		var fileNameStart = profilePicValue.lastIndexOf('\\');
		profilePicValue = profilePicValue.substr(fileNameStart + 1).substring(0, 20);
		//var profilePicLabelText = $(".upl"); /* finds the label text */
		if (profilePicValue != '') {
			//console.log($(this).closest('.fileUpload').find('.upl').length);
			$(this).closest('.fileUpload').find('.upl').html(profilePicValue);
		}
	});

	$(".btn-new").on('click', function () {
		$("#uploader").append('<div class="row uploadDoc"><div class="col-sm-3"><div class="docErr">Por favor, envie um arquivo válido</div><!--Erro!--><div class="fileUpload btn btn-orange"> <img src="https://image.flaticon.com/icons/svg/136/136549.svg" class="icon"><span class="upl" id="upload">Enviar Documento</span><input type="file" class="upload up" id="up" onchange="readURL(this);" /></div></div><div class="col-sm-8"><input type="text" class="form-control" name="" placeholder="Descrição"></div><div class="col-sm-1"><a class="btn-check"><i class="fa fa-times"></i></a></div></div>');
	});

	$(document).on("click", "a.btn-check", function () {
		if ($(".uploadDoc").length > 1) {
			$(this).closest(".uploadDoc").remove();
		} else {
			alert("Você deve enviar no mínimo 1 (um) Arquivo");
		}
	});
});