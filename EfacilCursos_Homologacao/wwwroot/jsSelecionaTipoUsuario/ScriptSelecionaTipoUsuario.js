﻿const studentCard = document.getElementById('student-card');
const teacherCard = document.getElementById('teacher-card');
const registerBtn = document.querySelector('.register-btn');

const enableRegistration = () => {
	registerBtn.removeAttribute('disabled');
	registerBtn.classList.add('enabled');
	customizeEnabledBtn();
};

const customizeEnabledBtn = () => {
	const enabledRegisterBtn = document.querySelector('.register-btn.enabled');
	enabledRegisterBtn.addEventListener('click', () => {
		enabledRegisterBtn.style.transform = 'scale(1.1)';
	});
};

studentCard.addEventListener('click', () => {
	window.location.replace("../Acesso/AlunoAdd");
	teacherCard.classList.remove('selected');
	studentCard.classList.add('selected');
	enableRegistration();
	toStudentScreen();
});

teacherCard.addEventListener('click', () => {
	window.location.replace("../Acesso/ProfessorAdd");
	studentCard.classList.remove('selected');
	teacherCard.classList.add('selected');
	enableRegistration();
	toTeacherScreen();
});

const toTeacherScreen = () => {
	const habiliteRegisterBtn = document.querySelector('.register-btn.enabled');
	habiliteRegisterBtn.addEventListener('click', () => {
		window.location.replace("../Acesso/ProfessorAdd");
	});
};

const toStudentScreen = () => {
	const habiliteRegisterBtn = document.querySelector('.register-btn.enabled');
	habiliteRegisterBtn.addEventListener('click', () => {
		window.location.replace("../Acesso/AlunoAdd");
	});
};