﻿const imageInputContainer = document.querySelector('.image-input');
const imageInput = document.getElementById('image');
const editImageBtn = document.getElementById('edit-image');
const deleteImageBtn = document.getElementById('delete-image');
// Alteração dos módulos
const editModuleBtns = document.querySelectorAll('#edit-pencil');
const saveModuleBtns = document.querySelectorAll('#save-edit');
/* Contagem de caracteres */
const textAreas = document.querySelectorAll('textarea');
/* Exclusão da aula */
const classCards = document.querySelectorAll('.class-card');
const modalContainer = document.querySelector('.delete-class-modal-container');
const reasonInput = document.getElementById('reason-input');
const confirmDeleteBtn = document.querySelector('.confirm-delete > button');
const cancelDeleteBtn = document.querySelector('.delete-class-btns > button');
const pendingClassModel = document.createElement('div');

imageInputContainer.addEventListener('click', () => {
	/* Caso já tenha sido feito o upload de uma imagem,
	 *  o click será desativado
	 */
	if (imageInputContainer.children.item(3)) {
		return;
	}
	imageInput.click();
});

imageInput.addEventListener('change', (e) => {
	const imageExists = imageInputContainer.children.item(3);
	if (imageExists) {
		imageExists.remove();
	}
	const selectedImage = e.target.files[0];

	if (!selectedImage) {
		imageInputContainer.classList.remove('preview');
		return;
	}
	const imageURL = URL.createObjectURL(selectedImage);
	const imgElement = document.createElement('img');
	imgElement.setAttribute('src', imageURL);
	imageInputContainer.classList.add('preview');
	imageInputContainer.appendChild(imgElement);
});

editImageBtn.addEventListener('click', () => {
	imageInput.click();
});

deleteImageBtn.addEventListener('click', () => {
	const imageExists = imageInputContainer.children.item(3);

	imageExists.remove();
	imageInputContainer.classList.remove('preview');
	imageInput.value = '';
});

saveModuleBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		const inputContainer = btn.parentElement;
		const inputs = inputContainer.querySelectorAll('input');

		inputs.forEach((input) => {
			input.setAttribute('disabled', true);
		});

		btn.style.opacity = '0';
		btn.style.cursor = 'default';
	});
});

editModuleBtns.forEach((btn) => {
	btn.addEventListener('click', () => {
		const inputContainer = btn.parentElement;
		const inputs = inputContainer.querySelectorAll('input');
		const saveIcon = inputContainer.querySelector('#save-edit');

		saveIcon.style.opacity = '1';
		saveIcon.style.cursor = 'pointer';

		inputs.forEach((input) => {
			input.removeAttribute('disabled');
		});
	});
});

textAreas.forEach((textArea) => {
	textArea.addEventListener('input', (e) => {
		const counter = textArea.nextElementSibling.querySelector('span');
		const valueLength = e.target.value.length;

		counter.innerText = valueLength;
	});
});

pendingClassModel.innerHTML = `<i class="far fa-clock"></i>
                                  <p>
                                    Pendente <br />
                                    a <br />
                                    exclusão
                                  </p>`;
pendingClassModel.classList.add('status-pending');

const closeModal = () => {
	modalContainer.classList.remove('active-aula');
	reasonInput.value = '';
	confirmDeleteBtn.setAttribute('disabled', true);
};

const changeStatus = (btn) => {
	const classCard = btn.parentElement;

	confirmDeleteBtn.addEventListener('click', () => {
		btn.remove();
		classCard.appendChild(pendingClassModel);
		closeModal();
	});
};

classCards.forEach((card) => {
	const deleteClassBtn = card.querySelector('#delete-class');

	deleteClassBtn.addEventListener('click', () => {
		changeStatus(deleteClassBtn);
		modalContainer.classList.add('active-aula');
	});
});

modalContainer.addEventListener('click', (e) => {
	if (e.target === modalContainer) {
		closeModal();
	}
});

cancelDeleteBtn.addEventListener('click', () => {
	closeModal();
});

reasonInput.addEventListener('input', (e) => {
	if (e.target.value.trim().length > 0) {
		confirmDeleteBtn.removeAttribute('disabled');
	} else {
		confirmDeleteBtn.setAttribute('disabled', true);
	}
});