﻿const modalContainer = document.querySelector('.modal-container');
const closeModal = document.getElementById('close-modal');

closeModal.addEventListener('click', () => {
	modalContainer.style.display = 'none';
});
