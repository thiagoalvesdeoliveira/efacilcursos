﻿const imageInputContainer = document.querySelector('.class-image-container');
const imageInput = document.getElementById('class-image');
const editImage = document.getElementById('edit-image-btn');
const deleteImage = document.getElementById('delete-image-btn');

/* Contagem de caracteres */
const descriptionInput = document.getElementById('class-description');
const counter = document.getElementById('character-counter');

/* Upload do vídeo */
const videoInputContainer = document.querySelector('.class-video-container');
const videoInput = document.getElementById('class-video');
const editVideo = document.getElementById('edit-video-btn');
const deleteVideo = document.getElementById('delete-video-btn');

imageInputContainer.addEventListener('click', () => {
	if (imageInputContainer.querySelector('img')) {
		return;
	}

	imageInput.click();
});

imageInput.addEventListener('change', (e) => {
	const existingImage = imageInputContainer.querySelector('img');

	if (existingImage) {
		existingImage.remove();
	}

	const selectedImage = e.target.files[0];

	if (!selectedImage) {
		imageInputContainer.classList.remove('preview');
		return;
	}
	const imageURL = URL.createObjectURL(selectedImage);
	const imagePreview = document.createElement('img');
	imagePreview.setAttribute('src', imageURL);
	imageInputContainer.appendChild(imagePreview);
	imageInputContainer.classList.add('preview');

	titleInput.value !== '' && imageInput.value !== ''
							&& descriptionInput.value !== '' 
							&& selectInput.value !== '0'
							&& videoInput.value !== '' ? createClassBtn.removeAttribute('disabled')
														: createClassBtn.setAttribute('disabled', true);
});

editImage.addEventListener('click', () => {
	imageInput.click();
});

deleteImage.addEventListener('click', () => {
	const existingImage = imageInputContainer.querySelector('img');
	existingImage.remove();
	imageInputContainer.classList.remove('preview');
	imageInput.value = '';

	// O listener de change não vê a exclusão, tive que colocar manualmente aqui
	createClassBtn.setAttribute('disabled', true);
});

descriptionInput.addEventListener('input', (e) => {
	const valueLength = e.target.value.length;
	counter.innerText = valueLength;
});

/* Verificação do título da aula */

const titleInput = document.getElementById('class-title');

titleInput.addEventListener('keypress', (e) => {
	const regex = /^[a-z\d\çãõâô\s]+$/i;
	const character = String.fromCharCode(e.which);

	if (!regex.test(character)) {
		e.preventDefault();
	}
});

videoInputContainer.addEventListener('click', () => {
	if (videoInputContainer.querySelector('video')) {
		return;
	}

	videoInput.click();
});

videoInput.addEventListener('change', (e) => {
	const existingVideo = videoInputContainer.querySelector('video');

	if (existingVideo) {
		existingVideo.remove();
	}

	const selectedVideo = e.target.files[0];

	if (!selectedVideo) {
		videoInputContainer.classList.remove('preview');
		return;
	}

	const videoURL = URL.createObjectURL(selectedVideo);
	const video = document.createElement('video');
	// video.setAttribute('poster', './assets/play.png');
	const videoSource = document.createElement('source');

	videoSource.setAttribute('src', `${videoURL}#t=15`);
	video.appendChild(videoSource);
	videoInputContainer.appendChild(video);

	video.addEventListener('click', () => {
		if (video.paused) {
			video.play();
		} else {
			video.pause();
		}
	});

	video.addEventListener('play', () => {
		if (video.currentTime === 15) {
			video.currentTime = 0;
		}
	});
	videoInputContainer.classList.add('preview');
	// Verifica se todos os inputs não estão vazios (por algum motivo só funciona se colocar aqui)
	titleInput.value !== '' &&
		imageInput.value !== '' &&
		descriptionInput.value !== '' &&
		selectInput.value !== '0' &&
		videoInput.value !== ''
		? createClassBtn.removeAttribute('disabled')
		: createClassBtn.setAttribute('disabled', true);
});

editVideo.addEventListener('click', () => {
	videoInput.click();
});

deleteVideo.addEventListener('click', () => {
	const existingVideo = videoInputContainer.querySelector('video');
	existingVideo.remove();
	videoInputContainer.classList.remove('preview');
	videoInput.value = '';
	// O listener de change não vê a exclusão, tive que colocar manualmente aqui
	createClassBtn.setAttribute('disabled', true);
});

/* Ativa o botão de criar */
const createClassBtn = document.querySelector('.create-class-btn');
const selectInput = document.getElementById('class-module');
const inputs = [titleInput, descriptionInput, selectInput];

inputs.forEach((input) => {
	input.addEventListener('change', () => {
		titleInput.value !== '' &&
			imageInput.value !== '' &&
			descriptionInput.value !== '' &&
			selectInput.value !== '0' &&
			videoInput.value !== ''
			? createClassBtn.removeAttribute('disabled')
			: createClassBtn.setAttribute('disabled', true);
	});
});