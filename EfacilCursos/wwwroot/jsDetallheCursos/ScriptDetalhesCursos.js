﻿function alertTeste() {
	Swal.fire({
		title:'Parabéns',
		text: 'Você adiquiriu um novo Curso!',
		icon: "success",
		showConfirmButton: false,
	}).then((result) => {
		if (result.value) {
			document.location.href = "../MeusCursos";
		}
	});
}