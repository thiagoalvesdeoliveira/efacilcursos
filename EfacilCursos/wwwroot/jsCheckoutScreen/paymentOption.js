﻿const creditCardRadio = document.getElementById('creditCard');
const paypalRadio = document.getElementById('paypal');
const creditCardPayment = document.querySelector('.payment-options-card');
const paypalPayment = document.querySelector('.payment-options-paypal');

// Mostrar os cartões de crédito registrados
creditCardRadio.addEventListener('change', () => {
	creditCardPayment.style.display = 'flex';
	paypalPayment.style.display = 'none';
});

// Mostrar as contas Paypal registradas
paypalRadio.addEventListener('change', () => {
	paypalPayment.style.display = 'flex';
	creditCardPayment.style.display = 'none';
});
