﻿const addNewCard = document.querySelector('.new-card');
const addNewPaypal = document.querySelector('.new-paypal');
const addNewCardForm = document.querySelector('.new-card-form');
const cancelButton = document.querySelector('.cancel-btn');

const cardNumber = document.getElementById('card-number');
const cardValityMonth = document.getElementById('select-month');
const cardValityYear = document.getElementById('select-year');
const cardSecurityNumber = document.getElementById('security-number');
const allCardsContainer = document.querySelector('.all-cards-here');
const containerTwo = document.querySelector('.all-cards-here.two');
const dropDown = document.querySelector('.fas.fa-chevron-down.dropdown');

// Array que guarda as informações básicas do cartão que serão mostradas em tela
let creditCards = [];

// Máscara do cartão
const cleave = new Cleave('.card-number', {
	creditCard: true,
});

const handleRegistrationNewCard = (e) => {
	e.preventDefault();

	const cardNumberValue = cardNumber.value;
	const cardValityMonthValue = cardValityMonth.value;
	const cardValityYearValue = cardValityYear.value;
	const cardSecurityNumberValue = cardSecurityNumber.value;

	// 4 últimos dígitos do cartão
	let lastDigits = cardNumberValue.slice(-4);

	const mainInfo = {
		lastDigits,
		valityMonth: cardValityMonthValue,
		valityYear: cardValityYearValue,
	};

	creditCards.push(mainInfo);
	updateDOM(creditCards);
};

// Atualiza a página, adicionando a ela o cartão adicionado e os outros já registrados
const updateDOM = (data) => {
	const cards = document.querySelectorAll('.card');
	if (cards.length >= 3) {
		const newData = data.filter((item, index) => index > 2 && item);
		addPagination(newData);
		return;
	}

	creditCardPayment.style.display = 'flex';
	addNewCardForm.style.display = 'none';
	paypalRadio.removeAttribute('disabled');
	// Reiniciando
	allCardsContainer.innerHTML = '';

	data.forEach((card) => {
		const element = document.createElement('div');
		element.classList.add('card');
		element.innerHTML = `<div class="flag-and-type">
       <img
         src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/363_Visa_Credit_Card_logo-512.png"
         alt=""
       />
       <p class="card-type">Débito/Crédito</p>
       <i class="fas fa-check-circle"></i>
     </div>
     <p class="card-number">
       **** **** **** ${card.lastDigits}
     </p>
     <p class="vality">
       Vence ${card.valityMonth}/${card.valityYear}
     </p>`;
		allCardsContainer.appendChild(element);

		element.addEventListener('click', () => {
			const registeredCards = document.querySelectorAll('.card');

			registeredCards.forEach((card) => {
				card.classList.remove('card-selected');
			});

			element.classList.toggle('card-selected');
		});
	});
};

const addPagination = (data) => {
	dropDown.style.display = 'block';

	creditCardPayment.style.display = 'flex';
	addNewCardForm.style.display = 'none';
	paypalRadio.removeAttribute('disabled');
	// Reiniciando
	containerTwo.innerHTML = '';

	data.forEach((card) => {
		const element = document.createElement('div');
		element.classList.add('card');
		element.innerHTML = `<div class="flag-and-type">
       <img
         src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/363_Visa_Credit_Card_logo-512.png"
         alt=""
       />
       <p class="card-type">Débito/Crédito</p>
       <i class="fas fa-check-circle"></i>
     </div>
     <p class="card-number">
       **** **** **** ${card.lastDigits}
     </p>
     <p class="vality">
       Vence ${card.valityMonth}/${card.valityYear}
     </p>`;
		containerTwo.appendChild(element);

		element.addEventListener('click', () => {
			const registeredCards = document.querySelectorAll('.card');

			registeredCards.forEach((card) => {
				card.classList.remove('card-selected');
			});

			element.classList.toggle('card-selected');
		});
	});
};

const registerNewCreditCard = () => {
	creditCardPayment.style.display = 'none';
	containerTwo.style.display = 'none';
	addNewCardForm.style.display = 'flex';
	paypalRadio.setAttribute('disabled', true);
};

const cancelRegisterNewCreditCard = () => {
	creditCardPayment.style.display = 'flex';
	containerTwo.style.display = 'flex';
	addNewCardForm.style.display = 'none';
	paypalRadio.removeAttribute('disabled');
};

// Mostra a form para registro de um novo cartão
addNewCard.addEventListener('click', registerNewCreditCard);

// Submit da form do cartão
addNewCardForm.addEventListener('submit', handleRegistrationNewCard);

// Cancela a exbição da form e voltar a mostrar os cartões
cancelButton.addEventListener('click', cancelRegisterNewCreditCard);

dropDown.addEventListener('click', () => {
	if (containerTwo.style.display === 'none') {
		containerTwo.style.display = 'flex';
		dropDown.classList.remove('fa-chevron-down');
		dropDown.classList.add('fa-chevron-up');
	} else {
		containerTwo.style.display = 'none';
		dropDown.classList.remove('fa-chevron-up');
		dropDown.classList.add('fa-chevron-down');
	}
});
