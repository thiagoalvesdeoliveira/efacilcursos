﻿const newPaypalAccount = document.querySelector('.new-paypal');

newPaypalAccount.addEventListener('click', () => {
	window.location.replace(
		'https://www.paypal.com/welcome/signup/#/email_password'
	);
});
