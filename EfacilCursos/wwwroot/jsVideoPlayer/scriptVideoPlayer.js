﻿var video = document.querySelector(".video");
var progressBar = document.querySelector(".orange-juice-bar");
var progressDiv = document.querySelector(".orange-bar-div");
var btn = document.getElementById("play-pause");


function togglePlayPause() {
	if (video.paused) {
		btn.className = 'pause';
		video.play();
	}
	else {
		btn.className = "play";
		video.pause();
	}
}

btn.onclick = function () {
	togglePlayPause();
};

video.addEventListener("timeupdate", function () {
	var juicePosition = video.currentTime / video.duration;
	progressBar.style.width = juicePosition * 100 + "%";
	if (video.ended) {
		btn.className = "play";
	}
});

function scrub(e) {
	const scrubTime = (e.offsetX / progressDiv.offsetWidth) * video.duration;
	video.currentTime = scrubTime;
}

let mousedown = false;
progressDiv.addEventListener("click", scrub);
progressDiv.addEventListener("mousemove", (e) => mousedown && scrub(e));
progressDiv.addEventListener("mousedown", () => mousedown = true);
progressDiv.addEventListener("mouseup", () => mousedown = false);

//Estas funcoes abaixo, trans o comportamento de fullscreen, porem esta ocorrendo um erro na API de fullScreen


//
//Primeira tentativa abaixo
//

//addEventListener("keypress", function (e) {
//	debugger;
//	if (e.keyCode === 102) {
//		toggleFullScreen();
//	}
//}, false);

//function toggleFullScreen() {
//	if (!document.fullscreenElement) {
//		document.documentElement.requestFullscreen();
//	} else {
//		if (document.exitFullscreen) {
//			document.exitFullscreen();
//		}
//	}
//}

//
//Segunda tentativa abaixo
//

//var p = document.querySelector('.video');

//if (!window.isFs) {
//	window.isFs = true;
//	var fn_enter = p.requestFullscreen || p.webkitRequestFullscreen || p.mozRequestFullScreen || p.oRequestFullscreen || p.msRequestFullscreen;
//	fn_enter.call(p);
//} else {
//	window.isFs = false;
//	var fn_exit = p.exitFullScreen || p.webkitExitFullScreen || p.mozExitFullScreen || p.oExitFullScreen || p.msExitFullScreen;
//	fn_exit.call(p);
//}