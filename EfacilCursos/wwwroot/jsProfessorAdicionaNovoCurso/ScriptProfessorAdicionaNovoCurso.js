﻿/* Upload da imagem */
const buttonCreate = document.querySelector('.create-course-btn');
const imageInputContainer = document.querySelector('.class-image-container');
const imageInput = document.getElementById('class-image');
const editImage = document.getElementById('edit-image-btn');
const deleteImage = document.getElementById('delete-image-btn');

// Contagem de caracteres
const textArea = document.getElementById("description");
const counter = document.getElementById("character-count");
//const characterCount = counter.querySelector("span");

// Regex
const twoInputsContainer = document.querySelectorAll(".two-inputs-container");
const alphaNumericsOnly = /[1234567890]/g;

imageInputContainer.addEventListener('click', () => {
	if (imageInputContainer.querySelector('img')) {
		return;
	}

	imageInput.click();
});

imageInput.addEventListener('change', (e) => {
	const existingImage = imageInputContainer.querySelector('img');

	if (existingImage) {
		existingImage.remove();
	}

	const selectedImage = e.target.files[0];

	if (!selectedImage) {
		imageInputContainer.classList.remove('preview');
		return;
	}

	const imageURL = URL.createObjectURL(selectedImage);
	const imagePreview = document.createElement('img');
	imagePreview.setAttribute('src', imageURL);
	imageInputContainer.appendChild(imagePreview);

	imageInputContainer.classList.add('preview');
});

editImage.addEventListener('click', () => {
	imageInput.click();
});

deleteImage.addEventListener('click', () => {
	const existingImage = imageInputContainer.querySelector('img');
	existingImage.remove();
	imageInputContainer.classList.remove('preview');
	imageInput.value = '';

	createClassBtn.setAttribute('disabled', true);
});

//textArea.addEventListener("input", (e) => {
//	const characters = e.target.value.length;
//	characterCount.innerText = characters;
//});

twoInputsContainer.forEach((container) => {
	const moduleInput = container.querySelector(":nth-child(2)");
	const classesInput = container.querySelector(":nth-child(4)");

	moduleInput.addEventListener("keypress", (e) => {
		const regex = /^[a-z\d\çãõâô\s]+$/i;
		const character = String.fromCharCode(e.which);

		if (!regex.test(character)) {
			e.preventDefault();
		}
	});

	classesInput.addEventListener("keypress", (e) => {
		const regex = /[0-9]/;
		const character = String.fromCharCode(e.which);

		if (!regex.test(character)) {
			e.preventDefault();
		}
	});
});

buttonCreate.addEventListener('click', () => {
	Swal.fire({
		title: 'Parabéns',
		text: 'Você adicionou um novo Curso!',
		icon: "success",
		showConfirmButton: false,
	});
});