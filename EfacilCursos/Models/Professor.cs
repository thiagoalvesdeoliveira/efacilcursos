﻿using EFacilCursos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EfacilCursos.Models
{
    public class Professor
    {
        [Key]
        public Guid Id_Professor { get; set; }        
        public string NomeProfessor { get; set; }
        public string CPF { get; set; }
        public string Profissao { get; set; }
        public string UplodsCursos { get; set; }
        public string AreaFormacao { get; set; }
        public string ExperienciaProfissional { get; set; }
        public string PlataforrmasProfessorUtilizaParaCurso { get; set; }
        public string TempoProfessorMinistrouAulas { get; set; }
        public string Sexo { get; set; }
        public string Url { get; set; }
        public DateTime DataCadastro{ get; set; }
        public DateTime DataNascimento{ get; set; }        
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Estado { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Cep { get; set; }
        public virtual ICollection<Cursos> ListaDeCursos { get; set; } = new List<Cursos>();
    }
}