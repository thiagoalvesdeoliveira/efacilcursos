﻿using System.ComponentModel.DataAnnotations;

namespace EFacilCursos.Models
{
    public class AcessoModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Senha { get; set; }

        public virtual string Tipo { get; set; }
    }
}
