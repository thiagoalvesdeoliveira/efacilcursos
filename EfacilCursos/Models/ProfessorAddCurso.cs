﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EfacilCursos.Models
{
    public class ProfessorAddCurso
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? CursoIdAdd { get; set; }
        public Guid? ProfessorId { get; set; }
    }
}
