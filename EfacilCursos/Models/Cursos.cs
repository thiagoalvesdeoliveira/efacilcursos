﻿using EFacilCursos.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace EfacilCursos.Models
{
    public class Cursos
    {
        [Key]
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string SubTitulo { get; set; }
        public string EnderecoImagem { get; set; }
        public string Descricao { get; set; }
        public Professor Professor { get; set; }         
        public string ProfessorId { get; set; }
        public string Nivel { get; set; }
        public string Idioma { get; set; }  
        public DateTime DataCadastro { get; set; }
        public bool Status { get; set; }
    }
}
