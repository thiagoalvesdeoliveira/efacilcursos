﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EfacilCursos.Models
{
    public class Aulas
    {
        [Key]
        public Guid Id_Aula { get; set; }
        public string  Titulo { get; set; }
        public string CaminhArquivo { get; set; }
        public string Descricao { get; set; }
        public string DesArquivo { get; set; }
        public string ModuloAula { get; set; }
        public string CaminhaoVideo { get; set; }
        public string CaminhaoImagem { get; set; }
        public string ProfessorId { get; set; }
        public DateTime DataCadastro { get; set; }
        public Cursos Curso { get; set; }
        public Guid CursoId { get; set; }
    }
}
