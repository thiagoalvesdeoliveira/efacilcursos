﻿using EfacilCursos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFacilCursos.Models
{
    public class Usuario : IValidatableObject
    {        
        [Key]
        public Guid Id_Aluno { get; set; }
        public string Nome { get; set; }       
        public string Email { get; set; }
        public string Senha { get; set; }
        public string CPF { get; set; }       
        public string Estado { get; set; }              
        public string Telefone { get; set; }
        public string Endereco { get; set; }        
        public string Cep { get; set; }        
        public string Sexo { get; set; }    
        public string Url { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataNascimento { get; set; }
        public bool Ativo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Enumerable.Empty<ValidationResult>();
        }        
    }
}
