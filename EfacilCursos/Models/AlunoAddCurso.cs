﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EfacilCursos.Models
{
    public class AlunoAddCurso
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? CursoId { get; set; }
        public Guid? AlunoId { get; set; }
    }
}
