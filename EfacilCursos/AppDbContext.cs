﻿using Microsoft.EntityFrameworkCore;
using EFacilCursos.Models;
using EfacilCursos.Models;

namespace EFacilCursos
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cursos> Cursos { get; set; }
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Aulas> Aulas { get; set; }
        public DbSet<AlunoAddCurso> AlunoAddCursos { get; set; }
        public DbSet<ProfessorAddCurso> ProfessorAddCurso { get; set; }
    }
}
