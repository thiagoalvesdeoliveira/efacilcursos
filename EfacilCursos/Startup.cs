﻿using System;
//using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EFacilCursos
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
              .AddCookie(options =>
              {
                  options.AccessDeniedPath = "/Acesso/SemPermissao";
                  options.LoginPath = "/Acesso/Index";
              });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
                options.Cookie.Name = ".Financeiro.Session";
            });


            //var conection = @"Server=DESKTOP-KCPS9FF\SQLEXPRESS;Database=EFacilCursos;Trusted_Connection=True";
            //var conection = "DESKTOP-0F1G1J9;Database=EFacilCursos;Trusted_Connection=True"; 
            //var conection = @"Server=DESKTOP-0F1G1J9\SQLEXPRESS;Database=EfacilCursos;Trusted_Connection=True";
            var conection = @"Server=DESKTOP-KCPs9FF\SQLEXPRESS;Database=EfacilCursos;Trusted_Connection=True";

            //string de cenexao
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(conection));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Acesso/Error");
            }

            app.UseAuthentication();
            app.UseSession();

            app.UseStaticFiles();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Acesso}/{action=LoginUsuario}/{id?}");
            });
        }
    }
}
