﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfacilCursos.Models;
using EFacilCursos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EfacilCursos.Controllers
{
    public class CursosController : Controller
    {
        private AppDbContext _Context;

        public CursosController(AppDbContext context)
        {
            _Context = context;
        }

        // GET: Cursos
        public ActionResult Index()
        {
            return View(_Context.Cursos.ToList());
        }

        //Modelo de grids
        public IActionResult IndexGrid(string search)
        {
            return PartialView(string.IsNullOrWhiteSpace(search) ? _Context.Cursos.AsQueryable() : _Context.Cursos.Where(c =>
                c.Nome.Contains(search) || c.Nome.Contains(search) || c.ProfessorId.ToString().Contains(search)));
        }

        // GET: Cursos/Edit/5
        public ActionResult Edit(Guid id)
        {
            if (id != null)
            {
                var cursos = _Context.Cursos.Include(c => c.ProfessorId).FirstOrDefault(c => c.Id == id);
                return View(cursos);
            }
            else
            {
                return View(new Cursos());
            }
        }

        // POST: Cursos/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(Cursos cursos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //if (cursos.Id != null)
                    //{
                    //    _Context.Entry(cursos).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    //    _Context.Entry(cursos).Property(x => x.DataCadastro).IsModified = false;
                    //    _Context.SaveChanges();

                    //    return RedirectToAction(nameof(Index));
                    //}
                    //else
                    //{
                    cursos.DataCadastro = DateTime.Now;
                    _Context.Cursos.Add(cursos);
                    _Context.SaveChanges();

                    return RedirectToAction("Index");
                    //}
                }
                else
                {
                    return View(cursos);
                }
            }
            catch (Exception ex)
            {
                return View(cursos);
            }
        }

        public ActionResult Delete(int id)
        {
            var persertir = _Context.Cursos.Find(id);
            _Context.Cursos.Remove(persertir);
            _Context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}