﻿using System;
using System.Linq;
using EfacilCursos.Models;
using EFacilCursos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EfacilCursos.Controllers
{
    public class ProfessorController : Controller
    {
        public class TerceirosController : Controller
        {
            private AppDbContext _Context;

            public TerceirosController(AppDbContext context)
            {
                _Context = context;
            }

            // GET: Professor
            public ActionResult Index()
            {
                return View(_Context.Professores.ToList());
            }

            //Modelo de grids
            public IActionResult IndexGrid(string search)
            {
                return PartialView(string.IsNullOrWhiteSpace(search) ? _Context.Professores.AsQueryable() : _Context.Professores.Where(c =>
                    c.NomeProfessor.Contains(search) || c.NomeProfessor.Contains(search) || c.Profissao.ToString().Contains(search)));
            }

            // GET: Professor/Edit/5
            public ActionResult Edit(Guid id)
            {
                var professores = _Context.Professores.Include(c => c.ListaDeCursos).FirstOrDefault(c => c.Id_Professor == id);
                return View(professores);
            }

            // POST: Professor/Edit/5
            [HttpPost]
            //[ValidateAntiForgeryToken]
            public ActionResult Edit(Professor professor)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        if (professor.NomeProfessor != null)
                        {
                            _Context.Entry(professor).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            _Context.Entry(professor).Property(x => x.DataCadastro).IsModified = false;
                            _Context.SaveChanges();

                            return RedirectToAction(nameof(Index));
                        }
                        else
                        {
                            professor.DataCadastro = DateTime.Now;
                            _Context.Professores.Add(professor);
                            _Context.SaveChanges();

                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        return View(professor);
                    }
                }
                catch (Exception ex)
                {
                    return View(professor);
                }
            }

            public ActionResult Delete(int id)
            {
                var persertir = _Context.Professores.Find(id);
                _Context.Professores.Remove(persertir);
                _Context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
        }
    }
}