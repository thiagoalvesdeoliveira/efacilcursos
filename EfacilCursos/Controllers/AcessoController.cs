﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using EFacilCursos.Models;
using Microsoft.EntityFrameworkCore;
using EfacilCursos.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;

namespace EFacilCursos.Controllers
{
    public class AcessoController : Controller
    {
        #region INSTÂNCIA IHostingEnvironment

        //Define uma instância de IHostingEnvironment
        IHostingEnvironment _appEnvironment;

        #endregion

        #region CONTEXT

        private AppDbContext _Context;

        public AcessoController(AppDbContext context, IHostingEnvironment env)
        {
            _Context = context;
            _appEnvironment = env;
        }
        #endregion

        #region LOGGING, LOGOUT  

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("LoginUsuario");
        }
        
        public ActionResult LoginUsuario(AcessoModel model)
        {
            if (model.Email != null)
            {
                try
                {
                    var professor = _Context.Professores.FirstOrDefault(c => c.Email == model.Email && c.Senha == model.Senha);
                    var usuario = _Context.Usuarios.FirstOrDefault(c => c.Email == model.Email && c.Senha == model.Senha);

                    if (professor != null)
                    {
                        model.Tipo = "professor";
                        return RedirectToAction("MeusCursosProfessor", model);                       
                    }
                    else if (usuario != null)
                    {
                        model.Tipo = "aluno";
                        return RedirectToAction("HomeCursos", model);                       
                    }
                    else
                    {
                        ViewBag.Messagen = "Email ou senha invalido.";
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("LoginUsuario", "Erro ao validar dados", ex);
                }
            }
            return View();
        }

        #endregion

        #region USUARIO

        public ActionResult Usuario()
        {
            return View();
        }

        public ActionResult AlunoAdd(int? id)
        {
            return View(new Usuario());
        }

        //Modelo de grids
        public IActionResult UsuarioGrid(string search)
        {
            return PartialView(string.IsNullOrWhiteSpace(search) ? _Context.Usuarios.AsQueryable() : _Context.Usuarios.Where(c =>
                c.Nome.Contains(search) ||
                c.Email.Contains(search)));
        }

        public ActionResult UsuarioEdit(int? id)
        {
            if (id > 0)
            {
                return View(_Context.Usuarios.Find(id));
            }
            else
            {
                return View(new Usuario());
            }
        }

        #endregion

        #region USUARIO EDIT AND ADD

        // POST: Usuario/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsuarioEdit(Usuario usuario)
        {
            try
            {
                var validacao = _Context.Usuarios.FirstOrDefault(c => c.Email == usuario.Email);

                if (validacao != null)
                {
                    ViewBag.Messagen = "Não foi possivel cadastrar Email, já é cadastrado na nossa base";
                    return RedirectToAction("AlunoAdd");
                }
                else
                {
                    usuario.Ativo = true;
                    usuario.DataCadastro = DateTime.Now;
                    usuario.Id_Aluno = Guid.NewGuid();

                    _Context.Usuarios.Add(usuario);
                    _Context.SaveChanges();

                    ViewBag.IdAluno = usuario;
                }
                return RedirectToAction("LoginUsuario");
            }
            catch (Exception ex)
            {
                return View(usuario);
            }
        }

        #endregion

        #region PLAYER VIDEO CURSO

        public ActionResult PlayerVideoCurso(Guid CursoId, Guid Id_Aula)
        {
            try
            {
                if (ModelState.IsValid && CursoId != null)
                {
                    var result = _Context.Cursos.FirstOrDefault(c => c.Id == CursoId);
                    var listaAulas = _Context.Aulas.Where(a => a.Id_Aula == Id_Aula).ToList();

                    ViewBag.AulaCurso = listaAulas;
                    ViewBag.Curso = result.Nome;

                    return View(ViewBag.AulaCurso);
                }
                return View();
            }
            catch (Exception)
            {
                BadRequest($"Não foi possivel localizar o curso pelo dados do professor {CursoId}");
            }
            return View();
        }

        #endregion

        #region AULAS CURSO

        [HttpGet]
        public ActionResult AulasCurso(Guid CursoId)
        {
            try
            {
                if (ModelState.IsValid && CursoId != null)
                {
                    var result = _Context.Cursos.FirstOrDefault(c => c.Id == CursoId);
                    var listaAulas = _Context.Aulas.Where(a => a.CursoId == result.Id).ToList();

                    ViewBag.AulaCurso = listaAulas;
                    ViewBag.AulaCursoCount = listaAulas.Count();
                    ViewBag.Curso = result.Nome;

                    return View(ViewBag.AulaCurso);
                }
                else
                {
                    return View(ViewBag.AulaCursoCount);
                }
            }
            catch (Exception ex)
            {
                BadRequest($"Não foi possivel localizar o curso pelo dados do professor {CursoId}");
            }
            return View();
        }

        public ActionResult AulasCurso(Guid CursoId, Guid AulasId)
        {
            try
            {
                if (ModelState.IsValid && CursoId != null)
                {
                    var result = _Context.Cursos.FirstOrDefault(c => c.Id == CursoId);
                    var listaAulas = _Context.Aulas.Where(a => a.CursoId == result.Id).ToList();

                    ViewBag.AulaCurso = listaAulas;
                    ViewBag.Curso = result.Nome;

                    return View(ViewBag.AulaCurso);
                }
                return View();
            }
            catch (Exception ex)
            {
                BadRequest($"Não foi possivel localizar o curso pelo dados do professor {CursoId}");

            }
            return View();
        }

        #endregion

        #region HOME CURSOS

        public ActionResult HomeCursos(AcessoModel model)
        {
            try
            {
                var listaDeCursos = _Context.Cursos.ToList();
                var listaDeAulas = _Context.Aulas.Count();

                ViewBag.AcessoModel = model.Tipo;
                ViewBag.AulasCount = listaDeAulas;
                ViewBag.CursosCount = listaDeCursos.Count();
                ViewBag.Cursos = listaDeCursos;

                return View(ViewBag.Cursos);
            }
            catch (Exception)
            {
                BadRequest($"Não foi possivel localizar cursos");
            }
            return View();
        }

        #endregion

        #region HOME CURSOS PROFESSOR

        public ActionResult HomeCursosProfessor(AcessoModel model)
        {
            try
            {
                var listaDeCursos = _Context.Cursos.ToList();
                var listaDeAulas = _Context.Aulas.Count();

                ViewBag.AcessoModel = model.Tipo;
                ViewBag.AulasCount = listaDeAulas;
                ViewBag.CursosCount = listaDeCursos.Count();
                ViewBag.Cursos = listaDeCursos;

                return View(ViewBag.Cursos);
            }
            catch (Exception)
            {
                BadRequest($"Não foi possivel localizar cursos");
            }
            return View();
        }

        #endregion

        #region MEUS CURSOS
        public ActionResult MeusCursos(Guid userId, Guid professorId, AcessoModel model, string modelUsuario)
        {
            try
            {
                //Aluno Id
                userId = new Guid("D41BEB25-E19F-4207-BDCC-7DAEF48D7213");
                var listaCursos = _Context.Cursos.ToList();

                var listaAlunoCurso = _Context.AlunoAddCursos.Where(c => c.AlunoId == userId).ToList();
                var listaDeCursoUsuario = (from itemAtual in listaAlunoCurso
                                           join itemAnterior in listaCursos
                                           on itemAtual.CursoId equals itemAnterior.Id
                                           select new { itemAnterior }).ToList();

                if (listaDeCursoUsuario.Any())
                {
                    ViewBag.Cursos = listaDeCursoUsuario.Select(c => c.itemAnterior).ToList();
                    ViewBag.CursosCount = listaDeCursoUsuario.Count();
                    ViewBag.AcessoModel = model.Tipo;

                    return View(ViewBag.Cursos);
                }
                else
                {
                    ViewBag.AcessoModel = model.Tipo;
                    return View();
                }
            }
            catch (Exception ex)
            {
                BadRequest($"Não foi possivel localizar o curso para o Usuario expecificado {userId}");
            }
            return View(model);
        }

        #endregion

        #region DETALHE CURSOS

        [HttpGet]
        public ActionResult DetalheCursos(Guid id)
        {            

            List<Cursos> cursosLista = new List<Cursos>();
            var cursoDetalhe = _Context.Cursos.Where(x => x.Id == id);

            cursosLista.Add(cursoDetalhe.FirstOrDefault());
            ViewBag.Curso = cursosLista;

            return View(ViewBag.Curso);
        }

        [HttpPost]
        public ActionResult DetalheCursos(Guid id, string Aluno_Id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!id.Equals(null))
                    {
                        AlunoAddCurso alunoAddCurso = new AlunoAddCurso();
                        alunoAddCurso.Id = Guid.NewGuid();
                        alunoAddCurso.AlunoId = new Guid("D41BEB25-E19F-4207-BDCC-7DAEF48D7213");//PcNatan
                        /*alunoAddCurso.AlunoId = new Guid("C0818D56-7DE7-4B53-5195-08D896F14D76");*///PcThiago
                        alunoAddCurso.CursoId = id;

                        _Context.AlunoAddCursos.Add(alunoAddCurso);
                        _Context.SaveChanges();

                        return RedirectToAction("MeusCursos");
                    }
                    else
                    {
                        return View($"Não foi possivel lovalizar as aulas do curos={id}", "MeusCursos");
                    }
                }
            }
            catch (Exception ex)
            {
                return View("MeusCursos");
            }
            return View("MeusCursos");
        }

        #endregion

        #region CHECKOUT

        public ActionResult CheckoutScreen()
        {
            return View();
        }

        #endregion

        #region SELECIONA TIPO CADASTRO USUARIO

        public ActionResult SelecionaTipoCadastroUsuario()
        {
            return View();
        }

        #endregion

        #region PROFESSOR-ADD
        public ActionResult ProfessorAdd(Guid? id)
        {
            if (id != null)
            {
                var professores = _Context.Professores.Include(c => c.ListaDeCursos).FirstOrDefault(c => c.Id_Professor == id);
                return View(professores);
            }
            else
            {
                return View(new Professor());
            }
        }

        [HttpPost]
        public ActionResult ProfessorAdd(Professor professor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    professor.DataCadastro = DateTime.Now;
                    professor.Id_Professor = Guid.NewGuid();
                    professor.UplodsCursos = "";
                    professor.Url = "";
                    professor.UplodsCursos = "";
                    _Context.Professores.Add(professor);
                    _Context.SaveChanges();

                    return RedirectToAction("LoginUsuario");
                }
                else
                {
                    return View(professor);
                }
            }
            catch (Exception ex)
            {
                return View(professor);
            }
        }
        #endregion

        #region GET CURSOS

        [HttpGet]
        //GetAllCursos
        public async Task<ActionResult> GetCursos(string professor_Id)
        {
            try
            {
                if (ModelState.IsValid && professor_Id != null)
                {
                    var result = await _Context.Cursos.FirstAsync(c => c.ProfessorId == professor_Id);
                    return View(result);
                }

                return View();
            }
            catch (Exception)
            {
                BadRequest($"Não foi possivel localizar o curso pelo dados do professor {professor_Id}");

            }

            return View();
        }

        public async Task<ActionResult> GetCursoAll()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _Context.Cursos.FirstAsync();
                    return View(result);
                }

                return View();
            }
            catch (Exception)
            {
                BadRequest($"Não foi possivel localizar o curso pelo dados do professor");

            }

            return View();
        }

        #endregion

        #region PROFESSOR ADICIONA CURSO

        public async Task<IActionResult> ProfessorAdicionaNovoCurso(Cursos curso, List<IFormFile> arquivos, AcessoModel model, string modelUsuario)
        {
            var imagem = "";
            var arquivoDoc = "";
            var nomeImagemParaBanco = "";

            if (arquivos.Count > 0)
            {
                long tamanhoArquivos = arquivos.Sum(f => f.Length);
                var caminhoArquivo = Path.GetTempFileName();
                var caminhoDestinoArquivo = "";

                foreach (var arquivo in arquivos)
                {
                    if (arquivo == null || arquivo.Length == 0)
                    {
                        ViewData["Erro"] = "Error: Arquivo(s) não selecionado(s)";
                        return View(ViewData);
                    }

                    string pasta = "Arquivos_Usuario";
                    string NomeImagem = arquivo.FileName;
                    string caminho_WebRoot = _appEnvironment.WebRootPath;

                    caminhoDestinoArquivo = caminho_WebRoot + "\\Arquivos\\" + pasta + "\\";
                    string caminhoDestinoArquivoOriginal = caminhoDestinoArquivo + "\\Recebidos\\" + NomeImagem;

                    using (var stream = new FileStream(caminhoDestinoArquivoOriginal, FileMode.Create))
                    {
                        await arquivo.CopyToAsync(stream);
                    }

                    ViewData["Resultado"] = $"{caminhoDestinoArquivo} Caminho do arquivo";

                    if (arquivo.ContentType.Contains("image"))
                    {
                        imagem = caminhoDestinoArquivo + NomeImagem;
                        nomeImagemParaBanco = NomeImagem;
                    }
                    if (arquivo.ContentType.Contains("application"))
                    {
                        arquivoDoc = caminhoDestinoArquivo;
                    }
                }
            }
            if (curso.Nome != null)
            {
                try
                {
                    if (ModelState.IsValid || curso.Descricao != null)
                    {
                        ProfessorAddCurso professorAddCurso = new ProfessorAddCurso();

                        //Passar um Id de professor gravado no banco de dados
                        curso.ProfessorId = "CD227B77-327E-4960-A7F8-0AE00141AD28";
                        curso.EnderecoImagem = nomeImagemParaBanco;
                        curso.Status = true;
                        curso.DataCadastro = DateTime.Now;
                        curso.Id = Guid.NewGuid();

                        professorAddCurso.Id = Guid.NewGuid();
                        professorAddCurso.ProfessorId = new Guid("CD227B77-327E-4960-A7F8-0AE00141AD28");
                        professorAddCurso.CursoIdAdd = curso.Id;
                           
                        

                        _Context.ProfessorAddCurso.Add(professorAddCurso);
                        _Context.Cursos.Add(curso);
                        _Context.SaveChanges();

                        var listaDeCursos = _Context.Cursos.ToList();
                        ViewBag.Cursos = listaDeCursos;

                        return RedirectToAction("HomeCursosProfessor");
                    }
                    else
                    {
                        return View(curso);
                    }
                }
                catch (Exception ex)
                {
                    return View(curso);
                }
            }
            return View();
        }

        #endregion

        #region PROFESSOR ADICIONA AULA

        public ActionResult ProfessorAdicionaAula(Aulas aula, Guid id, Guid idProfessor)
        {
            {
                if (id != null)
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            var curso = _Context.Cursos.FirstOrDefault(c => c.Id == id);

                            if (curso != null)
                            {
                                //Passar um Id de professor gravado no banco de dados
                                aula.ProfessorId = "9EABE9BE-E2DC-4903-90C3-AB395F4CE989";
                                aula.DataCadastro = DateTime.Now;
                                aula.Id_Aula = Guid.NewGuid();

                                _Context.Aulas.Add(aula);
                                _Context.SaveChanges();

                                return View(aula);
                            }
                            else
                            {
                                return View($"Não foi possivel localizar o curso");
                            }
                        }
                        else
                        {
                            return View("ProfessorAdicionaAula");
                        }
                    }
                    catch (Exception)
                    {
                        return View("ProfessorAdicionaAula");
                    }
            }
            return View("MeusCursosProfessor");
        }

        [HttpPost]
        [RequestSizeLimit(134217728)]
        public async Task<IActionResult> ProfessorAdicionaAula(Aulas aula, string moduloCurso, string descricaoAnexo, Guid idProfessor, List<IFormFile> arquivos, Guid id)
        {
            var imagem = "";
            var video = "";
            var arquivoDoc = "";
            var nomeArquivoParaBanco = "";
            var nomeImagemParaBanco = "";
            var nomeVideoParaBanco = "";

            if (arquivos.Count > 0)
            {
                long tamanhoArquivos = arquivos.Sum(f => f.Length);
                var caminhoArquivo = Path.GetTempFileName();
                var caminhoDestinoArquivo = "";

                foreach (var arquivo in arquivos)
                {
                    if (arquivo == null || arquivo.Length == 0)
                    {
                        ViewData["Erro"] = "Error: Arquivo(s) não selecionado(s)";
                        return View(ViewData);
                    }

                    string pasta = "Arquivos_Usuario";
                    string nomeArquivo = arquivo.FileName;
                    string caminho_WebRoot = _appEnvironment.WebRootPath;

                    caminhoDestinoArquivo = caminho_WebRoot + "\\Arquivos\\" + pasta + "\\";
                    string caminhoDestinoArquivoOriginal = caminhoDestinoArquivo + "\\Recebidos\\" + nomeArquivo;

                    using (var stream = new FileStream(caminhoDestinoArquivoOriginal, FileMode.Create))
                    {
                        await arquivo.CopyToAsync(stream);
                    }

                    ViewData["Resultado"] = $"{caminhoDestinoArquivo} Caminho do arquivo";

                    if (arquivo.ContentType.Contains("image"))
                    {
                        imagem = caminhoDestinoArquivo;
                        nomeImagemParaBanco = nomeArquivo;
                    }
                    if (arquivo.ContentType.Contains("video"))
                    {
                        video = caminhoDestinoArquivo;
                        nomeVideoParaBanco = nomeArquivo;
                    }
                    if (arquivo.ContentType.Contains("application"))
                    {
                        arquivoDoc = caminhoDestinoArquivo;
                        nomeArquivoParaBanco = nomeArquivo;
                    }
                }
            }
            if (aula.Titulo != null)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        //Passar um Id de professor gravado no banco de dados
                        aula.ProfessorId = "CD227B77-327E-4960-A7F8-0AE00141AD28";
                        aula.CursoId = id;
                        aula.CaminhArquivo = nomeArquivoParaBanco;
                        aula.CaminhaoVideo = nomeVideoParaBanco;
                        aula.CaminhaoImagem = nomeImagemParaBanco;
                        aula.ModuloAula = moduloCurso;
                        aula.DesArquivo = descricaoAnexo;
                        aula.DataCadastro = DateTime.Now;
                        aula.Id_Aula = Guid.NewGuid();

                        _Context.Aulas.Add(aula);
                        _Context.SaveChanges();

                        return RedirectToAction("MeusCursosProfessor");
                    }
                    else
                    {
                        return View(aula);
                    }
                }
                catch (Exception ex)
                {
                    return View(aula);
                }
            }
            return View(aula);
        }

        #endregion

        #region MEUS CURSOS PROFESSOR

        public ActionResult MeusCursosProfessor()
        {
            var userId = new Guid("960038E6-33F7-42AD-B3BD-D747A91BA4E1");
            var professorId = new Guid("CD227B77-327E-4960-A7F8-0AE00141AD28");

            var listaCursos = _Context.Cursos.ToList();

            var listaProfessorCurso = _Context.ProfessorAddCurso.Where(c => c.ProfessorId == professorId).ToList();
            var listaDeCursoProfessor = (from itemAtual in listaProfessorCurso
                                         join itemAnterior in listaCursos
                                         on itemAtual.CursoIdAdd equals itemAnterior.Id
                                         select new { itemAnterior }).ToList();

            if (listaDeCursoProfessor.Any())
            {
                ViewBag.Cursos = listaDeCursoProfessor.Select(c => c.itemAnterior).ToList();
                ViewBag.CursosCount = listaDeCursoProfessor.Count();
            }
            return View(ViewBag.Cursos);
        }

        #endregion

        #region PROFESSOR ALTERA AULA

        public ActionResult ProfessorAlteraAula()
        {
            return View();
        }

        #endregion

        #region TESTE ARQUIVO ENVIA

        public async Task<IActionResult> EnviarArquivo(List<IFormFile> arquivos)
        {
            if (arquivos.Count > 0)
            {
                long tamanhoArquivos = arquivos.Sum(f => f.Length);
                var caminhoArquivo = Path.GetTempFileName();
                var caminhoDestinoArquivo = "";

                foreach (var arquivo in arquivos)
                {
                    if (arquivo == null || arquivo.Length == 0)
                    {
                        ViewData["Erro"] = "Error: Arquivo(s) não selecionado(s)";
                        return View(ViewData);
                    }

                    string pasta = "Arquivos_Usuario";
                    string nomeArquivo = "Usuario_arquivo_" + DateTime.Now.Millisecond.ToString();

                    if (arquivo.FileName.Contains(".jpg"))
                        nomeArquivo += ".jpg";
                    else if (arquivo.FileName.Contains(".gif"))
                        nomeArquivo += ".gif";
                    else if (arquivo.FileName.Contains(".png"))
                        nomeArquivo += ".png";
                    else if (arquivo.FileName.Contains(".pdf"))
                        nomeArquivo += ".pdf";
                    else
                        nomeArquivo += ".tmp";

                    string caminho_WebRoot = _appEnvironment.WebRootPath;
                    caminhoDestinoArquivo = caminho_WebRoot + "\\Arquivos\\" + pasta + "\\";
                    string caminhoDestinoArquivoOriginal = caminhoDestinoArquivo + "\\Recebidos\\" + nomeArquivo;

                    using (var stream = new FileStream(caminhoDestinoArquivoOriginal, FileMode.Create))
                    {
                        await arquivo.CopyToAsync(stream);
                    }
                }
                ViewData["Resultado"] = $"{caminhoDestinoArquivo} Caminho do arquivo";
            }
            return View(ViewData);
        }

        #endregion

        #region QUANDO NÃO HÁ CURSO CADASTRADO

        public ActionResult NaoHaCursoCadastrado()
        {
            return View();
        }

        #endregion

        #region CURSO CRIADO COM SUCESSO

        public ActionResult CursoCriadoSucesso()
        {
            return View();
        }

        #endregion
    }
}