﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EfacilCursos.Controllers
{
    public class AlunoAddCursos : Controller
    {
        // GET: AlunoAddCursos
        public ActionResult Index()
        {
            return View();
        }

        // GET: AlunoAddCursos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AlunoAddCursos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AlunoAddCursos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AlunoAddCursos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AlunoAddCursos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AlunoAddCursos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AlunoAddCursos/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
