USE [EfacilCursos]
GO

/****** Object:  Table [dbo].[AlunoAddCursos]    Script Date: 11/12/2020 23:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AlunoAddCursos](
	[Id] [uniqueidentifier] NOT NULL,
	[AlunoId] [uniqueidentifier] NULL,
	[CursoId] [uniqueidentifier] NULL,
 CONSTRAINT [pk_id_aluno] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AlunoAddCursos]  WITH CHECK ADD  CONSTRAINT [FK_cursos] FOREIGN KEY([CursoId])
REFERENCES [dbo].[Cursos] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AlunoAddCursos] CHECK CONSTRAINT [FK_cursos]
GO

ALTER TABLE [dbo].[AlunoAddCursos]  WITH CHECK ADD  CONSTRAINT [FK_usuarios] FOREIGN KEY([AlunoId])
REFERENCES [dbo].[Usuarios] ([Id_Aluno])
GO

ALTER TABLE [dbo].[AlunoAddCursos] CHECK CONSTRAINT [FK_usuarios]
GO