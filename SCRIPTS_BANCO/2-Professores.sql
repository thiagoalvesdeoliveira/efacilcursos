USE [EfacilCursos]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Professores](
	[Id_Professor] [uniqueidentifier] NOT NULL,
	[NomeProfessor] [nvarchar](100) NOT NULL,
	[CPF] [nvarchar](11) NOT NULL,
	[Profissao] [nvarchar](40) NOT NULL,
	[UplodsCursos] [nvarchar](30) NULL,
	[AreaFormacao] [nvarchar](40) NOT NULL,
	[ExperienciaProfissional] [nvarchar](40) NOT NULL,
	[PlataforrmasProfessorUtilizaParaCurso] [nvarchar](40) NULL,
	[TempoProfessorMinistrouAulas] [nvarchar](10) NOT NULL,
	[Sexo] [nvarchar](1) NOT NULL,
	[Url] [nvarchar](40) NULL,
	[DataCadastro] [date] NOT NULL,
	[DataNascimento] [date] NOT NULL,
	[Email] [nvarchar](40) NOT NULL,
	[Senha] [nvarchar](max) NOT NULL,
	[Estado] [nvarchar](10) NOT NULL,
	[Telefone] [nvarchar](11) NOT NULL,
	[Endereco] [nvarchar](11) NOT NULL,
	[Cep] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_Professores] PRIMARY KEY CLUSTERED 
(
	[Id_Professor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO