USE [EfacilCursos]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuarios](
	[Id_Aluno] [uniqueidentifier] NOT NULL,
	[Nome] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](320) NOT NULL,
	[Senha] [nvarchar](100) NOT NULL,
	[CPF] [nvarchar](11) NOT NULL,
	[Estado] [nvarchar](100) NOT NULL,
	[Telefone] [nvarchar](100) NOT NULL,
	[Endereco] [nvarchar](100) NOT NULL,
	[Cep] [nvarchar](18) NOT NULL,
	[Sexo] [nvarchar](18) NOT NULL,
	[Url] [nvarchar](600) NULL,
	[DataCadastro] [date] NOT NULL,
	[DataNascimento] [date] NOT NULL,
	[Ativo] [bit] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Id_Aluno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO