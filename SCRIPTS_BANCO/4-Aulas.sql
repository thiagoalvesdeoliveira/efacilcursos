USE [EfacilCursos]
GO

/****** Object:  Table [dbo].[Aulas]    Script Date: 11/12/2020 23:09:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Aulas](
	[Id_Aula] [uniqueidentifier] NOT NULL,
	[Titulo] [nvarchar](max) NULL,
	[CaminhArquivo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[DesArquivo] [nvarchar](max) NULL,
	[ModuloAula] [nvarchar](max) NULL,
	[CaminhaoVideo] [nvarchar](max) NULL,
	[CaminhaoImagem] [nvarchar](max) NULL,
	[ProfessorId] [nvarchar](max) NULL,
	[DataCadastro] [datetime2](7) NOT NULL,
	[CursoId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Aulas] PRIMARY KEY CLUSTERED 
(
	[Id_Aula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Aulas]  WITH CHECK ADD  CONSTRAINT [FK_Aulas_Cursos_CursoId] FOREIGN KEY([CursoId])
REFERENCES [dbo].[Cursos] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Aulas] CHECK CONSTRAINT [FK_Aulas_Cursos_CursoId]
GO