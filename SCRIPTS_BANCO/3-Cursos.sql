USE [EfacilCursos]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cursos](
	[Id] [uniqueidentifier] NOT NULL,
	[Nome] [nvarchar](max) NULL,
	[SubTitulo] [nvarchar](max) NULL,
	[EnderecoImagem] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[ProfessorId_Professor] [uniqueidentifier] NULL,
	[ProfessorId] [nvarchar](max) NULL,
	[Nivel] [nvarchar](max) NULL,
	[Idioma] [nvarchar](max) NULL,
	[DataCadastro] [datetime2](7) NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Cursos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Cursos]  WITH CHECK ADD  CONSTRAINT [FK_Cursos_Professores_ProfessorId_Professor] FOREIGN KEY([ProfessorId_Professor])
REFERENCES [dbo].[Professores] ([Id_Professor])
GO

ALTER TABLE [dbo].[Cursos] CHECK CONSTRAINT [FK_Cursos_Professores_ProfessorId_Professor]
GO