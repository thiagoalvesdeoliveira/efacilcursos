USE [EfacilCursos]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProfessorAddCurso](
	[Id] [uniqueidentifier] NOT NULL,
	[ProfessorId] [uniqueidentifier] NULL,
	[CursoIdAdd] [uniqueidentifier] NULL,
 CONSTRAINT [pk_Id_Professor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProfessorAddCurso]  WITH CHECK ADD  CONSTRAINT [FK_cursosAdd] FOREIGN KEY([CursoIdAdd])
REFERENCES [dbo].[Cursos] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProfessorAddCurso] CHECK CONSTRAINT [FK_cursosAdd]
GO

ALTER TABLE [dbo].[ProfessorAddCurso]  WITH CHECK ADD  CONSTRAINT [FK_professores] FOREIGN KEY([ProfessorId])
REFERENCES [dbo].[Professores] ([Id_Professor])
GO

ALTER TABLE [dbo].[ProfessorAddCurso] CHECK CONSTRAINT [FK_professores]
GO